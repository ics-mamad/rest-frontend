import { useState } from 'react'
import {DELETE_USER} from '../GraphQL/Mutations'
import {useMutation} from '@apollo/client'

const Deleteusers = () => {

    const [deleteUser,{error}] = useMutation(DELETE_USER)
    const [email,setEmail] = useState('')
    const Delete = () => {

        deleteUser({
            variables:{
                email:email
            }
        })
        if(error){
            alert(error)
        }
        setEmail('')
        console.log(email)
        // alert("User Deleted Successfully !!!")
    }

  return (
    <div><b>Delete User :</b> <br />
    ID : <input type="text" placeholder='Enter Email ID To Delete' onChange={(e)=>setEmail(e.target.value)}/><br /><br />
    <button onClick={Delete}>Delete User</button>
    </div>
  )
}

export default Deleteusers
