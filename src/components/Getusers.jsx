import {useEffect, useState} from 'react'
import {useQuery} from '@apollo/client'
import {GET_ME} from '../GraphQL/Queries'

const Getusers = () => {

    const {error,loading,data} = useQuery(GET_ME)
    const [user,setUser] = useState([])


    useEffect(()=>{
      if(data){
        console.log(data)
        setUser(data.getUsers)
      }
    },[data])
    
    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error : {error.message}</p>;

  return (
    <div><b>Users :</b> 
      {user.map((val)=>{
        return <p key={val.email}>{val.name} - {val.email}</p>
      })}
    </div>
  )
}

export default Getusers
