import {gql} from "@apollo/client";

export const GET_ME = gql`
  query{
    getUsers{
        name
        age
        gender
        email
        password
    }
  }
`