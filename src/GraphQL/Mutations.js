import { gql } from "@apollo/client";

export const CREATE_USER = gql`

mutation createUser($name: String! $age: Int! $gender: String! $email: String! $password: String!){
    createUser(name: $name age: $age gender: $gender email: $email password: $password){
        name,age,gender,email,password
    }
}`

export const DELETE_USER = gql`

mutation deleteUser($email:String!){
    deleteUser(email:$email){
        name
    }
}`

export const UPDATE_USER = gql`

mutation updateUser($id:ID! $name: String $age: Int $gender: String $email: String $password: String){
    updateUser(id:$id name: $name age: $age gender: $gender email: $email password: $password){
        name,age,gender,email,password
    }
}

`
